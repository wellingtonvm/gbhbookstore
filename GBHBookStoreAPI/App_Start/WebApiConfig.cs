﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace GBHBookStoreAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "BooksByAuthor",
                routeTemplate: "api/books/author/{authorid}",
                defaults: new {
                    controller="Books",
                    action= "Author",
                }
            );

            config.Routes.MapHttpRoute(
                name: "BooksByCategory",
                routeTemplate: "api/books/category/{categoryid}",
                defaults: new
                {
                    controller = "Books",
                    action = "Category",
                }
            );

            config.Routes.MapHttpRoute(
                name: "BooksByFormat",
                routeTemplate: "api/books/format/{formatid}",
                defaults: new
                {
                    controller = "Books",
                    action = "Format",
                }
            );

            config.Routes.MapHttpRoute(
                name: "BooksByLanguage",
                routeTemplate: "api/books/language/{languageid}",
                defaults: new
                {
                    controller = "Books",
                    action = "Language",
                }
            );

            config.Routes.MapHttpRoute(
                name: "BookPages",
                routeTemplate: "api/books/pages/{bookid}",
                defaults: new
                {
                    controller = "Books",
                    action = "Pages",
                }
            );

            config.Routes.MapHttpRoute(
                name: "CategoriesByBook",
                routeTemplate: "api/categories/book/{bookid}",
                defaults: new
                {
                    controller = "Categories",
                    action = "Book",
                }
            );

            config.Routes.MapHttpRoute(
                name: "FormatsByBook",
                routeTemplate: "api/formats/book/{bookid}",
                defaults: new
                {
                    controller = "Formats",
                    action = "Book",
                }
            );

            config.Routes.MapHttpRoute(
                name: "LanguagesByBook",
                routeTemplate: "api/languages/book/{bookid}",
                defaults: new
                {
                    controller = "Languages",
                    action = "Book",
                }
            );
        }
    }
}
