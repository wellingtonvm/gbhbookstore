﻿using GBHBookStoreAPI.Models;
using GBHBookStoreAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GBHBookStoreAPI.Controllers
{
    public class AuthorsController : ApiController
    {
        AuthorsRepository myRepo = new AuthorsRepository(new ApplicationDbContext());

        public IQueryable<Author> GetAll()
        {
            return myRepo.GetAll().AsQueryable();
        }
    }
}
