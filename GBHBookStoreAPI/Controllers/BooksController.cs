﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GBHBookStoreAPI.Models;
using GBHBookStoreAPI.Repositories;
namespace GBHBookStoreAPI.Controllers
{
    public class BooksController : ApiController
    {
        BooksRepository myRepo = new BooksRepository(new ApplicationDbContext());

        public IQueryable<Book> GetAll()
        {
            return myRepo.GetAll().AsQueryable();
        }

        [HttpGet]
        public IQueryable<Book> Author(int authorid)
        {
            return myRepo.GetBooksByAuthorID(authorid).AsQueryable();
        }

        [HttpGet]
        public IQueryable<Book> Category(int categoryid)
        {
            return myRepo.GetBooksByCategoryID(categoryid).AsQueryable();
        }

        [HttpGet]
        public IQueryable<Book> Format(int formatid)
        {
            return myRepo.GetBooksByFormatID(formatid).AsQueryable();
        }

        [HttpGet]
        public IQueryable<Book> Language(int languageid)
        {
            return myRepo.GetBooksByLanguageID(languageid).AsQueryable();
        }

        [HttpGet]
        public IQueryable<BookPage> Pages(int bookid)
        {
            return myRepo.GetBookPages(bookid).AsQueryable();
        }
    }
}
