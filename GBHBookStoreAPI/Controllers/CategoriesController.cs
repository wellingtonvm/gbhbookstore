﻿using GBHBookStoreAPI.Models;
using GBHBookStoreAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GBHBookStoreAPI.Controllers
{
    public class CategoriesController : ApiController
    {
        CategoriesRepository myRepo = new CategoriesRepository(new ApplicationDbContext());

        public IQueryable<Category> GetAll()
        {
            return myRepo.GetAll().AsQueryable();
        }

        [HttpGet]
        public IQueryable<Category> Book(int bookid)
        {
            return myRepo.GetAllCategoriesByBookID(bookid).AsQueryable();
        }
    }
}
