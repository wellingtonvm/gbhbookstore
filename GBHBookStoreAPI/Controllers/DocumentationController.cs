﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GBHBookStoreAPI.Controllers
{
    public class DocumentationController : Controller
    {
        // GET: Documentation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Books()
        {
            return View();
        }

        public ActionResult Categories()
        {
            return View();
        }

        public ActionResult Authors()
        {
            return View();
        }

        public ActionResult Formats()
        {
            return View();
        }

        public ActionResult Languages()
        {
            return View();
        }
    }
}