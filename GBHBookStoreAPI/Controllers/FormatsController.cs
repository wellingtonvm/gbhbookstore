﻿using GBHBookStoreAPI.Models;
using GBHBookStoreAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GBHBookStoreAPI.Controllers
{
    public class FormatsController : ApiController
    {
        FormatsRepository myRepo = new FormatsRepository(new ApplicationDbContext());

        public IQueryable<Format> GetAll()
        {
            return myRepo.GetAll().AsQueryable();
        }

        [HttpGet]
        public IQueryable<BookFormat> Book(int bookid)
        {
            return myRepo.GetAllFormatsByBookID(bookid).AsQueryable();
        }
    }
}
