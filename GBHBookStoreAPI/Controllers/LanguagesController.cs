﻿using GBHBookStoreAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GBHBookStoreAPI.Models;

namespace GBHBookStoreAPI.Controllers
{
    public class LanguagesController : ApiController
    {
        LanguagesRepository myRepo = new LanguagesRepository(new ApplicationDbContext());

        public IQueryable<Language> GetAll()
        {
            return myRepo.GetAll().AsQueryable();
        }

        [HttpGet]
        public IQueryable<Language> Book(int bookid)
        {
            return myRepo.GetAllLanguagesByBookID(bookid).AsQueryable();
        }
    }
}
