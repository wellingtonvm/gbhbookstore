﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GBHBookStoreAPI.Models;
namespace GBHBookStoreAPI.Interfaces
{
    interface IBookStoreRepository
    {
        /// <summary>
        /// Get the all the books catalog
        /// </summary>
        /// <returns>List of all books</returns>
        List<Book> GetAllBooks();

        /// <summary>
        /// Get a book by specific ID
        /// </summary>
        /// <param name="id">Book ID</param>
        /// <returns>Book object</returns>
        Book GetBookByID(int id);

        /// <summary>
        /// Return the list of books by an author id
        /// </summary>
        /// <param name="id">author id</param>
        /// <returns>List of books</returns>
        List<Book> GetBooksByAuthorID(int id);

        /// <summary>
        /// Return the list of books by category id
        /// </summary>
        /// <param name="id">category id</param>
        /// <returns>List of books</returns>
        List<Book> GetBooksByCategoryID(int id);

        /// <summary>
        /// Return the list of books by format id
        /// </summary>
        /// <param name="id">format id</param>
        /// <returns>List of books</returns>
        List<Book> GetBooksByFormatID(int id);

        /// <summary>
        /// Return the list of books by language id
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>List of books</returns>
        List<Book> GetBooksByLanguageID(int id);

        /// <summary>
        /// Return the pages of a book by book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>List of book pages</returns>
        List<BookPage> GetBookPages(int id);

        /// <summary>
        /// Get the list of authors
        /// </summary>
        /// <returns>List of authors</returns>
        List<Author> GetAllAuthors();

        /// <summary>
        /// Get Author by ID
        /// </summary>
        /// <param name="id">Author ID </param>
        /// <returns>Auhthor object</returns>
        Author GetAuthorByID(int id);

        /// <summary>
        /// Get the list of categories
        /// </summary>
        /// <returns>List of categories</returns>
        List<Category> GetAllCategories();

        /// <summary>
        /// Return a list of categories related of a book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>list of categories</returns>
        List<Category> GetAllCategoriesByBookID(int id);

        /// <summary>
        /// Get the list of formats
        /// </summary>
        /// <returns>List of formats</returns>
        List<Format> GetAllFormats();

        /// <summary>
        /// Return a list of formats related of a book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>list of formats</returns>
        List<BookFormat> GetAllFormatsByBookID(int id);

        /// <summary>
        /// Get the list of languanges
        /// </summary>
        /// <returns>List of languages</returns>
        List<Language> GetAllLanguages();

        /// <summary>
        /// Return a list of languages related of a book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>list of languages</returns>
        List<Language> GetAllLanguagesByBookID(int id);

    }
}
