namespace GBHBookStoreAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelsCreationAndSeeding : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        AuthorID = c.Int(nullable: false, identity: true),
                        AuthorName = c.String(nullable: false),
                        Description = c.String(),
                        AuthorPhoto = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AuthorID);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        BookID = c.Int(nullable: false, identity: true),
                        AuthorID = c.Int(nullable: false),
                        BookTitle = c.String(nullable: false),
                        Description = c.String(),
                        PublicationDate = c.DateTime(nullable: false),
                        ISBN = c.String(),
                        CoverImage = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BookID)
                .ForeignKey("dbo.Authors", t => t.AuthorID, cascadeDelete: true)
                .Index(t => t.AuthorID);
            
            CreateTable(
                "dbo.BooksCategories",
                c => new
                    {
                        BookCategoryID = c.Int(nullable: false, identity: true),
                        BookID = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        IsCategoryFeatured = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BookCategoryID)
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.BookID)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(nullable: false),
                        Description = c.String(),
                        CategoryCoverImage = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.BooksFormats",
                c => new
                    {
                        BookFormatID = c.Int(nullable: false, identity: true),
                        BookID = c.Int(nullable: false),
                        FormatID = c.Int(nullable: false),
                        PriceUSD = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.BookFormatID)
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Formats", t => t.FormatID, cascadeDelete: true)
                .Index(t => t.BookID)
                .Index(t => t.FormatID);
            
            CreateTable(
                "dbo.Formats",
                c => new
                    {
                        FormatID = c.Int(nullable: false, identity: true),
                        FormatName = c.String(nullable: false),
                        FormatIcon = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.FormatID);
            
            CreateTable(
                "dbo.BooksLanguages",
                c => new
                    {
                        BookLanguageID = c.Int(nullable: false, identity: true),
                        BookID = c.Int(nullable: false),
                        LanguageID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BookLanguageID)
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .Index(t => t.BookID)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        LanguageID = c.Int(nullable: false, identity: true),
                        LanguageName = c.String(nullable: false),
                        LanguageIcon = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.LanguageID);
            
            CreateTable(
                "dbo.BooksPages",
                c => new
                    {
                        BookPageID = c.Int(nullable: false, identity: true),
                        BookID = c.Int(nullable: false),
                        PageIndex = c.Int(nullable: false),
                        PlainTextContent = c.String(nullable: false),
                        HtmlContent = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.BookPageID)
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .Index(t => t.BookID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.BooksPages", "BookID", "dbo.Books");
            DropForeignKey("dbo.BooksLanguages", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.BooksLanguages", "BookID", "dbo.Books");
            DropForeignKey("dbo.BooksFormats", "FormatID", "dbo.Formats");
            DropForeignKey("dbo.BooksFormats", "BookID", "dbo.Books");
            DropForeignKey("dbo.BooksCategories", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.BooksCategories", "BookID", "dbo.Books");
            DropForeignKey("dbo.Books", "AuthorID", "dbo.Authors");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.BooksPages", new[] { "BookID" });
            DropIndex("dbo.BooksLanguages", new[] { "LanguageID" });
            DropIndex("dbo.BooksLanguages", new[] { "BookID" });
            DropIndex("dbo.BooksFormats", new[] { "FormatID" });
            DropIndex("dbo.BooksFormats", new[] { "BookID" });
            DropIndex("dbo.BooksCategories", new[] { "CategoryID" });
            DropIndex("dbo.BooksCategories", new[] { "BookID" });
            DropIndex("dbo.Books", new[] { "AuthorID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.BooksPages");
            DropTable("dbo.Languages");
            DropTable("dbo.BooksLanguages");
            DropTable("dbo.Formats");
            DropTable("dbo.BooksFormats");
            DropTable("dbo.Categories");
            DropTable("dbo.BooksCategories");
            DropTable("dbo.Books");
            DropTable("dbo.Authors");
        }
    }
}
