namespace GBHBookStoreAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;
    internal sealed class Configuration : DbMigrationsConfiguration<GBHBookStoreAPI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GBHBookStoreAPI.Models.ApplicationDbContext context)
        {
            context.Languages.AddOrUpdate(
              l => l.LanguageName,
              new Language { LanguageID=1, LanguageName = "English",LanguageIcon="english.png" },
              new Language { LanguageID = 2, LanguageName = "Spanish", LanguageIcon = "spanish.png" },
              new Language { LanguageID = 3, LanguageName = "French", LanguageIcon = "french.png" }
            );

            context.Formats.AddOrUpdate(
              f => f.FormatName,
              new Format { FormatID=1, FormatName = "Audiobook", FormatIcon = "audiobook.png" },
              new Format { FormatID = 2, FormatName = "Ebook", FormatIcon = "ebook.png" },
              new Format { FormatID = 3, FormatName = "Kindle", FormatIcon = "kindle.png" },
              new Format { FormatID = 4, FormatName = "PDF", FormatIcon = "pdf.png" },
              new Format { FormatID = 5, FormatName ="Printed",FormatIcon="printed.png" }
            );

            context.Categories.AddOrUpdate(
              c=>c.CategoryName,              
              new Category { CategoryID=1, CategoryName = "Kids", CategoryCoverImage = "kids.png", Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sem augue, accumsan a metus id, volutpat aliquam nulla. Mauris malesuada imperdiet nisi ut euismod. Phasellus pharetra justo in tristique tristique." },
              new Category { CategoryID = 2, CategoryName = "Christian", CategoryCoverImage = "christian.png", Description = "" },              
              new Category { CategoryID = 3, CategoryName = "History", CategoryCoverImage = "history.png", Description = "Fusce sit amet ante in nisi tristique laoreet. Maecenas cursus blandit est vitae scelerisque. Pellentesque condimentum eros lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce nec fringilla eros. Aenean vel risus diam." },
              new Category { CategoryID = 4, CategoryName = "Fiction", CategoryCoverImage = "fiction.png", Description = "Nulla at luctus est. Integer consequat ornare libero a convallis. Fusce ac rutrum ex." },
              new Category { CategoryID = 5, CategoryName = "Romance", CategoryCoverImage = "romance.png", Description = "Nulla at luctus est. Integer consequat ornare libero a convallis. Fusce ac rutrum ex." },
              new Category { CategoryID = 6, CategoryName = "Sci-Fi", CategoryCoverImage = "scifi.png", Description = "Etiam sem augue, accumsan a metus id, volutpat aliquam nulla. Mauris malesuada imperdiet nisi ut euismod." },
              new Category { CategoryID = 7, CategoryName = "Social Sciences", CategoryCoverImage = "social.png", Description = "Etiam sem augue, accumsan a metus id, volutpat aliquam nulla. Mauris malesuada imperdiet nisi ut euismod." },
              new Category { CategoryID = 8, CategoryName = "Short Stories", CategoryCoverImage = "stories.png", Description = "Etiam sem augue, accumsan a metus id, volutpat aliquam nulla. Mauris malesuada imperdiet nisi ut euismod." }
            );

            context.Authors.AddOrUpdate(
                a=>a.AuthorName,
                new Author { AuthorID=1, AuthorName="Juan Bosch",Description= "Juan Emilio Bosch Gavi�o was a Dominican politician, historian, short story writer, essayist, educator, and the first democratically elected president of the Dominican Republic for a brief time in 1963. ", AuthorPhoto="bosch.jpg" },
                new Author { AuthorID = 2, AuthorName = "Truman Capote", Description = "Truman Garcia Capote was an American novelist, short story writer, screenwriter, playwright, and actor. ", AuthorPhoto = "truman.jpg" },
                new Author { AuthorID = 3, AuthorName = "Edgar Allan Poe", Description = "Edgar Allan Poe was an American writer, editor, and literary critic. Poe is best known for his poetry and short stories, particularly his tales of mystery and the macabre.", AuthorPhoto = "allanpoe.jpg" },
                new Author { AuthorID = 4, AuthorName = "Stephen King", Description = "tephen Edwin King is an American author of horror, supernatural fiction, suspense, science fiction, and fantasy.", AuthorPhoto = "stephenking.jpg" },
                new Author { AuthorID = 5, AuthorName = "Rita Indiana", Description = "Rita Indiana Hern�ndez S�nchez is a Dominican writer, and singer-songwriter. In 2011, she was selected by the newspaper El Pa�s as one of the 100 most influential Latino personalities. ", AuthorPhoto = "ritaindiana.jpg" }
            );

            context.Books.AddOrUpdate(
               b=>b.BookTitle,
               new Book { BookID=1, BookTitle="In Cold Blood",Description= "On November 15, 1959, in the small town of Holcomb, Kansas, four members of the Clutter family were savagely murdered by blasts from a shotgun held a few inches from their faces. There was no apparent motive for the crime, and there were almost no clues.", ISBN= "978-0679745587", CoverImage="incoldblood.jpg",PublicationDate=Convert.ToDateTime("01/01/1966"), AuthorID=2 },
               new Book { BookID = 2, BookTitle = "Breakfast at Tiffany's and Three Stories", Description = "In this seductive, wistful masterpiece, Truman Capote created a woman whose name has entered the American idiom and whose style is a part of the literary landscape. Holly Golightly knows that nothing bad can ever happen to you at Tiffany's; her poignancy, wit, and na�vet� continue to charm.", ISBN = "978-0679745655", CoverImage = "breakfastattif.jpg", PublicationDate = Convert.ToDateTime("01/01/1958"), AuthorID = 2 },
               new Book { BookID = 3, BookTitle = "The Tell-Tale Heart and Other Stories", Description = "Edgar Allan Poe elevated the gothic story, developed the unreliable narrator, recast psychological terror, and reveled in both the horror and the supernal beauty of death. From Poe's rich, unrivaled imagination comes a collection of his most masterful works", ISBN = "B079V2NTLP", CoverImage = "thetelltale.jpg", PublicationDate = Convert.ToDateTime("03/27/2018"), AuthorID = 3 },
               new Book { BookID = 4, BookTitle = "The Raven", Description = "Narrative poem by the American writer and poet Edgar Allan Poe. It was published for the first time on January 29, 1845, in the New York Evening Mirror. Noted for its musicality, stylized language and supernatural atmosphere, it tells of the mysterious visit of a talking raven to a distraught lover, tracing his slow descent into madness.", ISBN = "B07FK9YP5M", CoverImage = "theraven.jpg", PublicationDate = Convert.ToDateTime("07/11/2018"), AuthorID = 3 },
               new Book { BookID = 5, BookTitle = "The Fall of the House of Usher and Other Tales", Description = "This selection of Poe's critical writings, short fiction and poetry demonstrates an intense interest in aesthetic issues and the astonishing power and imagination with which he probed the darkest corners of the human mind.", ISBN = "B002RI9I0G", CoverImage = "thehouseusher.jpg", PublicationDate = Convert.ToDateTime("03/27/2003"), AuthorID = 3 },
               new Book { BookID = 6, BookTitle = "Judas Iscariote, el Calumniado", Description = "El escritor y pol�tico Juan Bosch dedic� sus mejores dotes literarias a la reivindicaci�n de la figura hist�rica de Judas Iscariote. El rigor y la amenidad de su escritura, elogiada por escritores como Gabriel Garc�a M�rquez y Pablo Neruda, han convertido este libro en un cl�sico de la literatura historiogr�fica.", ISBN = "B011VU39ZU", CoverImage = "judas.jpg", PublicationDate = Convert.ToDateTime("07/17/2015"), AuthorID = 1 },
               new Book { BookID = 7, BookTitle = "La manosa", Description = "La Ma�osa: Es una obra narrativa, prol�fica y literaria escrita por Juan Bosch, que habla sobre la vida de unos campesinos durante la revoluciones armadas y todas las dificultades que pasaron durante el tiempo que se desarrolla la historia. En esta obra existe un contexto social e hist�rico donde Juan, un muchacho pobre de padres muy respetados y muy conocidos en el pueblo que encontr�ndose enfermo durante la revoluci�n, es quien narra en la obra lo que vivi�.", ISBN = "B01CCALG3K", CoverImage = "lamanosa.jpg", PublicationDate = Convert.ToDateTime("02/28/2016"), AuthorID = 1 },
               new Book { BookID = 8, BookTitle = "Papi", Description = "A girl waits for her father, a Dominican mobster whom she idolizes and who always shows up without warning. All throughout her life her daddy appears, disappears, and reappears, laden with money and an endless supply of cars, girlfriends, and presents. ", ISBN = "978-8492865406", CoverImage = "papi.jpg", PublicationDate = Convert.ToDateTime("11/28/2012"), AuthorID = 5 },
               new Book { BookID = 9, BookTitle = "The Outsider", Description = "An unspeakable crime. A confounding investigation. At a time when the King brand has never been stronger, he has delivered one of his most unsettling and compulsively readable stories.", ISBN = "978-1501180989", CoverImage = "outsider.jpg", PublicationDate = Convert.ToDateTime("05/22/2018"), AuthorID = 4 },
               new Book { BookID = 10, BookTitle = "1922", Description = "A violence awakens inside a man when his wife proposes selling off the family homestead, setting in motion a grisly train of murder and madness.", ISBN = "B075X2GCLB", CoverImage = "1922.jpg", PublicationDate = Convert.ToDateTime("10/13/2017"), AuthorID = 4 },
               new Book { BookID = 11, BookTitle = "Under the dome", Description = "Tight and energetic from start to finish... Hard as this thing is to hoist, it's even harder to put down.", ISBN = "978-1476735474", CoverImage = "underdome.jpg", PublicationDate = Convert.ToDateTime("06/11/2013"), AuthorID = 4 }
            );

            //Tagging for each book

            
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 1, CategoryID = 7, IsCategoryFeatured = true },
                new BookCategory { BookID = 1, CategoryID = 3, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 1, FormatID = 3, PriceUSD = 11.99 },
                new BookFormat { BookID = 1, FormatID = 5, PriceUSD = 12.80 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 1, LanguageID = 1 },
                new BookLanguage { BookID = 1, LanguageID = 3 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID=1,PageIndex=1,HtmlContent="Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.",PlainTextContent="Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 1, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 1, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 1, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 1, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 1, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 1, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 1, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 1, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 1, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 2, CategoryID = 8, IsCategoryFeatured = true }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 2, FormatID = 3, PriceUSD = 9.99 },
                new BookFormat { BookID = 2, FormatID = 1, PriceUSD = 0.00 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 2, LanguageID = 1 },
                new BookLanguage { BookID = 2, LanguageID = 2 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 2, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 2, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 2, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 2, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 2, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 2, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 2, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 2, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 2, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 2, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--

            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 3, CategoryID = 8, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 3, FormatID = 5, PriceUSD = 6.99 },
                new BookFormat { BookID = 3, FormatID = 2, PriceUSD = 1.00 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 3, LanguageID = 1 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 3, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 3, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 3, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 3, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 3, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 3, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 3, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 3, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 3, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 3, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--

            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 4, CategoryID = 4, IsCategoryFeatured = true }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 4, FormatID = 5, PriceUSD = 4.99 },
                new BookFormat { BookID = 4, FormatID = 2, PriceUSD = 1.00 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 4, LanguageID = 1 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 4, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 4, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 4, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 4, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 4, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 4, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 4, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 4, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 4, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 4, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 5, CategoryID = 8, IsCategoryFeatured = true }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 5, FormatID = 5, PriceUSD = 2.83 },
                new BookFormat { BookID = 5, FormatID = 3, PriceUSD = 1.99 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 5, LanguageID = 1 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 5, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 5, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 5, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 5, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 5, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 5, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 5, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 5, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 5, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 5, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--

            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 6, CategoryID = 2, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 6, FormatID = 5, PriceUSD = 99 },
                new BookFormat { BookID = 6, FormatID = 3, PriceUSD = 6.99 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 6, LanguageID = 2 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 6, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 6, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 6, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 6, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 6, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 6, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 6, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 6, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 6, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 6, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 7, CategoryID = 3, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 7, FormatID = 3, PriceUSD = 6.00 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 7, LanguageID = 2 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 7, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 7, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 7, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 7, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 7, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 7, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 7, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 7, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 7, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 7, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 8, CategoryID = 7, IsCategoryFeatured = true },
                new BookCategory { BookID = 8, CategoryID = 3, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 8, FormatID = 5, PriceUSD = 20.12 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 8, LanguageID = 2 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 8, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 8, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 8, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 8, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 8, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 8, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 8, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 8, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 8, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 8, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 9, CategoryID = 4, IsCategoryFeatured = false },
                new BookCategory { BookID = 9, CategoryID = 6, IsCategoryFeatured = true },
                new BookCategory { BookID = 9, CategoryID = 3, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 9, FormatID = 5, PriceUSD = 17.99 },
                new BookFormat { BookID = 9, FormatID = 3, PriceUSD = 11.03 },
                new BookFormat { BookID = 9, FormatID = 2, PriceUSD = 5.00 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 9, LanguageID = 1 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 9, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 9, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 9, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 9, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 9, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 9, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 9, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 9, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 9, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 9, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 10, CategoryID = 4, IsCategoryFeatured = true },
                new BookCategory { BookID = 10, CategoryID = 3, IsCategoryFeatured = false }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 10, FormatID = 3, PriceUSD = 4.99 },
                new BookFormat { BookID = 10, FormatID = 1, PriceUSD = 1.00 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 10, LanguageID = 1 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 10, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 10, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 10, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 10, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 10, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 10, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 10, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 10, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 10, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 10, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );
            //--
            context.BooksCategories.AddOrUpdate(
                new BookCategory { BookID = 11, CategoryID = 6, IsCategoryFeatured = true }
                );

            context.BooksFormats.AddOrUpdate(
                new BookFormat { BookID = 11, FormatID = 5, PriceUSD = 12.99 }
                );

            context.BooksLanguages.AddOrUpdate(
                new BookLanguage { BookID = 11, LanguageID = 1 }
                );

            context.BooksPages.AddOrUpdate(
                new BookPage { BookID = 11, PageIndex = 1, HtmlContent = "Ipsum voluptua sit diam ipsum aliquyam elitr stet sed stet nonummy soluta ea at augue dolores esse facilisi et nulla.", PlainTextContent = "Ipsum ipsum elitr diam vero sea lorem ex et consetetur ullamcorper laoreet at aliquip facer luptatum dolor dolor clita consequat hendrerit ea sanctus ea accusam no elitr sit laoreet dolore" },
                new BookPage { BookID = 11, PageIndex = 2, HtmlContent = "Noi prieghi esso dio nome priegano novellare le da che dallo dovendo a le durare udita e sue e maravigliose e cosa che pi� come non in noi manifestamente bene", PlainTextContent = "Facciamo quale fa potremmo di riguardando impetrata cose sua i impermutabile non facciamo s� niuno che con che cosa iscacciato iscacciato quella cose mescolati giudicio tanto avvedimento che che da" },
                new BookPage { BookID = 11, PageIndex = 3, HtmlContent = "Was times time shun ear childe rill soils of now made haply were that thence more perchance and the kiss shrine not feud in these such and agen by to who a he by or vile haply feere in delight", PlainTextContent = "Which or sullen into wight all vexed below than change when and mote call then pleasure not sullen me was or then childe blast not there change pillared despair him each worse might fame that flow he cell break old" },
                new BookPage { BookID = 11, PageIndex = 4, HtmlContent = "Getr�umt w�rest heut bester rast du immer dahinten verschwiegen mir's liebe rast jedoch schnelle liebe fort heimat doch so getr�umt der niedlich du dich schnee im sp�t und mich da du die dann die grambefangen ihr sanftes sanft die ich mein du und teuren gerne dir der schwester sie im", PlainTextContent = "Werden doch um o zur�ck deinen an weiter und ich mich es o im geliebet manchmal denkst im der ward gef�rbt geliebet dich sanft dich treibt erg�tzt laue herzen z�rntest wangen weiter verschwand sp�t du und grambefangen der manchmal ankleiden ich tr�ume nicht schnee gl�ck schmilzt geschaut nacht weiter gehn" },
                new BookPage { BookID = 11, PageIndex = 5, HtmlContent = "Lichens balottant nus d'ineffables b�ni mes tohu-bohus ne ou qu'on et un d'or et le lunules �cumes ou dont sais des et mer ivre ont m'ont flache qu'on accroupi violets", PlainTextContent = "Reculons des sapin et a libre de ouverts qui les - acteurs pareils l'homme mes un soir les niais mufle" },
                new BookPage { BookID = 11, PageIndex = 6, HtmlContent = "Aussi divinement morceau la myst�rieux au ce �la ce l'el�gance fin vivre gr�ces mensonge femme regard et surtout termine morceau", PlainTextContent = "Crisp�e s'abreuve l'art s'abreuve et a jusqu'aux chaque atrocement myst�rieux yeux a apres-demain monstre pontife et bic�phale langoureux a vainqueur" },
                new BookPage { BookID = 11, PageIndex = 7, HtmlContent = "Into if that lent lie into more front shall of burned pallas flown dying usby quoth gaunt of he rapping", PlainTextContent = "Outpour he air grew turning and soul bust hopes be methought i purple and the the into at the that" },
                new BookPage { BookID = 11, PageIndex = 8, HtmlContent = "De por pulso algod�n estremecidos en ligeros las m�rmol diana las para diana muerte tu desangradas sus llanura aire pero", PlainTextContent = "Luna que brooklyn y para se ni la pies arcos pisan los quedo hombrecillo nino pensamiento cielo transparente me de" },
                new BookPage { BookID = 11, PageIndex = 9, HtmlContent = "Il soeurs mal vaincu demain visage miraculeux a gentillesse corps et son extase son v�ritable grande langoureux fatale d�cor trait", PlainTextContent = "Yg ne halal kyniuhhad bel uiraga ualmun uilaga scegenul ulud kynaal fugwa therthetyk kyt num ezes myth hullothya scouuo egembelu" },
                new BookPage { BookID = 11, PageIndex = 10, HtmlContent = "Sur et ce d'une d'un h�las de coeur promene voici la faite fleuve voici de myst�rieux demain langoureux gr�ces tete", PlainTextContent = "Las en abrir dios se quedo que de la paso llanura del del los queman tierra de que borrachos y" }
                );


        }
    }
}
