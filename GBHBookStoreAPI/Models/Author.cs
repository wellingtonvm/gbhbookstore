﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GBHBookStoreAPI.Models
{
    [Table("Authors")]
    public class Author
    {
        [Key]
        public int AuthorID { get; set; }
        [Display(Name ="Author Name")]
        [Required(ErrorMessage ="Author name is required")]
        public string AuthorName { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [Display(Name = "Author Photo")]
        [Required(ErrorMessage = "Photo is required")]
        public string AuthorPhoto { get; set; }
        //public ICollection<Book> AuthorBooks { get; set; }

    }
}