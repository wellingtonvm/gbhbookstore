﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GBHBookStoreAPI.Models
{
    [Table("Books")]
    public class Book
    {
        [Key]
        public int BookID { get; set; }
        public int AuthorID { get; set; }
        [Display(Name = "Book Name")]
        [Required(ErrorMessage = "Name is required")]
        public string BookTitle { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [Display(Name = "Publication Date")]
        [Required(ErrorMessage = "Date is required")]
        public DateTime PublicationDate { get; set; }
        public string ISBN { get; set; }
        [Display(Name = "Cover Image")]
        [Required(ErrorMessage = "Cover image is required")]
        public string CoverImage { get; set; } 
        [JsonIgnore]
        public virtual Author Author { get; set; }
    }
}