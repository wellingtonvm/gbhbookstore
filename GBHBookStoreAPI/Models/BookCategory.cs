﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Models
{
    [Table("BooksCategories")]
    public class BookCategory
    {
        [Key]
        public int BookCategoryID { get; set; }
        public int BookID { get; set; }
        public int CategoryID { get; set; }
        [Display(Name ="Featured on Category")]
        public bool IsCategoryFeatured { get; set; }

        public virtual Book Book { get; set; }
        public virtual Category Category { get; set; }
    }
}