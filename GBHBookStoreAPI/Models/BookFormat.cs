﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Models
{
    [Table("BooksFormats")]
    public class BookFormat
    {
        [Key]
        public int BookFormatID { get; set; }
        public int BookID { get; set; }
        public int FormatID { get; set; }
        [Display(Name ="Price USD $")]
        [Required(ErrorMessage ="You must supply a price for each book format")]
        public double PriceUSD { get; set; }

        public virtual Book Book { get; set; }
        public virtual Format Format { get; set; }
    }
}