﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Models
{
    [Table("BooksLanguages")]
    public class BookLanguage
    {
        [Key]
        public int BookLanguageID { get; set; }
        public int BookID { get; set; }
        public int LanguageID { get; set; }

        public virtual Book Book { get; set; }
        public virtual Language Language { get; set; }
    }
}