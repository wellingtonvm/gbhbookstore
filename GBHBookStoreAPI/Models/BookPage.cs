﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GBHBookStoreAPI.Models
{
    [Table("BooksPages")]
    public class BookPage
    {
        [Key]
        public int BookPageID { get; set; }
        public int BookID { get; set; }
        public int PageIndex { get; set; }
        [Display(Name = "Plain Text Page Content")]
        [Required(ErrorMessage = "Plain text is required")]
        public string PlainTextContent { get; set; }
        [AllowHtml]
        [Display(Name = "Html Page Content")]
        [Required(ErrorMessage = "Html is required")]
        public string HtmlContent { get; set; }

        public virtual Book Book { get; set; }
    }
}