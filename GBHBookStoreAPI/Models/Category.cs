﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GBHBookStoreAPI.Models
{
    [Table("Categories")]
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        [Display(Name ="Category Name")]
        [Required(ErrorMessage ="Name is required for the category")]
        public string CategoryName { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [Display(Name = "Cover Image")]
        [Required(ErrorMessage = "Cover Image is required for the category")]
        public string CategoryCoverImage { get; set; }

        //public ICollection<BookCategory> CategoryBooks { get; set; }
    }
}