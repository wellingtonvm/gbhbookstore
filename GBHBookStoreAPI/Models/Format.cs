﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Models
{
    [Table("Formats")]
    public class Format
    {
        [Key]
        public int FormatID { get; set; }
        [Display(Name ="Format Name")]
        [Required(ErrorMessage ="Format name is required")]
        public string FormatName { get; set; }
        [Display(Name = "Format Icon")]
        [Required(ErrorMessage = "Format icon is required")]
        public string FormatIcon { get; set; }

        //public ICollection<BookFormat> FormatBooks { get; set; }
    }
}