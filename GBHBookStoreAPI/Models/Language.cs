﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Models
{
    [Table("Languages")]
    public class Language
    {
        [Key]
        public int LanguageID { get; set; }
        [Display(Name ="Language Name")]
        [Required(ErrorMessage ="Name is required")]
        public string LanguageName { get; set; }
        [Display(Name = "Icon File")]
        [Required(ErrorMessage = "Icon is required")]
        public string LanguageIcon { get; set; }

        //public ICollection<BookLanguage> LanguageBooks { get; set; }
    }
}