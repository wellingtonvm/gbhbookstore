﻿using GBHBookStoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Repositories
{
    public class AuthorsRepository : EntityBaseRepository<Author>
    {
        public AuthorsRepository(ApplicationDbContext context) : base(context) { }

    }
}