﻿using GBHBookStoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Repositories
{
    public class BooksRepository : EntityBaseRepository<Book>
    {
        ApplicationDbContext db;
        public BooksRepository(ApplicationDbContext context) : base(context)
        {
            db = context;
        }


        /// <summary>
        /// Return the pages of a book by book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>List of book pages</returns>
        public List<BookPage> GetBookPages(int id)
        {
            return db.BooksPages.Where(p => p.BookID == id).OrderBy(p => p.PageIndex).ToList();
        }

        /// <summary>
        /// Return the list of books by an author id
        /// </summary>
        /// <param name="id">author id</param>
        /// <returns>List of books</returns>
        public List<Book> GetBooksByAuthorID(int id)
        {
            return db.Books.Where(a => a.AuthorID == id).OrderBy(b => b.BookTitle).ToList();
        }

        /// <summary>
        /// Return the list of books by category id
        /// </summary>
        /// <param name="id">category id</param>
        /// <returns>List of books</returns>
        public List<Book> GetBooksByCategoryID(int id)
        {
            List<Book> result = new List<Book>();
            List<BookCategory> bc = db.BooksCategories.Include("Book").Where(c => c.CategoryID == id).ToList();
            foreach (var item in bc)
            {
                result.Add(item.Book);
            }
            return result;
        }

        /// <summary>
        /// Return the list of books by format id
        /// </summary>
        /// <param name="id">format id</param>
        /// <returns>List of books</returns>
        public List<Book> GetBooksByFormatID(int id)
        {
            List<Book> result = new List<Book>();
            List<int> booksids = (from c in db.BooksFormats
                                  where c.FormatID == id
                                  select c.BookID).ToList();
            return db.Books.Where(b => booksids.Contains(b.BookID)).ToList();
        }

        /// <summary>
        /// Return the list of books by language id
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>List of books</returns>
        public List<Book> GetBooksByLanguageID(int id)
        {
            List<Book> result = new List<Book>();
            List<BookLanguage> bc = db.BooksLanguages.Include("Book").Where(c => c.LanguageID == id).ToList();
            foreach (var item in bc)
            {
                result.Add(item.Book);
            }
            return result;
        }
    }
}