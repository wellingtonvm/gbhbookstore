﻿using GBHBookStoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Repositories
{
    public class CategoriesRepository : EntityBaseRepository<Category>
    {
        ApplicationDbContext db;
        public CategoriesRepository(ApplicationDbContext context) : base(context)
        {
            db = context;
        }

        /// <summary>
        /// Return a list of categories related of a book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>list of categories</returns>
        public List<Category> GetAllCategoriesByBookID(int id)
        {
            List<Category> result = new List<Category>();
            List<BookCategory> bc = db.BooksCategories.Include("Category").Where(c => c.BookID == id).ToList();
            foreach (var item in bc)
            {
                result.Add(item.Category);
            }
            return result;
        }
    }
}