﻿using GBHBookStoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Repositories
{
    public class FormatsRepository : EntityBaseRepository<Format>
    {
        ApplicationDbContext db;
        public FormatsRepository(ApplicationDbContext context) : base(context)
        {
            db = context;
        }

        /// <summary>
        /// Return a list of formats related of a book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>list of formats</returns>
        public List<BookFormat> GetAllFormatsByBookID(int id)
        {

            return db.BooksFormats.Include("Format").Where(c => c.BookID == id).ToList();

        }
    }
}