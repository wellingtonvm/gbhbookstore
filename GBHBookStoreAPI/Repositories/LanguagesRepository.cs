﻿using GBHBookStoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GBHBookStoreAPI.Repositories
{
    public class LanguagesRepository : EntityBaseRepository<Language>
    {
        ApplicationDbContext db;
        public LanguagesRepository(ApplicationDbContext context) : base(context)
        {
            db = context;
        }

        /// <summary>
        /// Return a list of languages related of a book id
        /// </summary>
        /// <param name="id">book id</param>
        /// <returns>list of languages</returns>
        public List<Language> GetAllLanguagesByBookID(int id)
        {
            List<Language> result = new List<Language>();
            List<BookLanguage> bc = db.BooksLanguages.Include("Language").Where(c => c.BookID == id).ToList();
            foreach (var item in bc)
            {
                result.Add(item.Language);
            }
            return result;
        }
    }
}